﻿using System.Text;
using System.Windows;
using System.ComponentModel;

namespace D12
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public static d12_core d12core;

        private int aim;
        private int mv;

        /// <summary>
        /// Тип прицеливания(Кол-во Д12)
        /// </summary>
        public int Aim
        {
            get
            {
                return aim;
            }

            set
            {
                if (aim != value)
                {
                    aim = value < 0 ? 0 : value;
                    OnPropertyChanged("Aim");
                }
            }
        }

        /// <summary>
        /// Перемещение (Номер меремещения)
        /// </summary>
        public int Mv
        {
            get
            {
                return mv;
            }

            set
            {
                if (mv != value)
                {
                    mv = value < 0 ? 0 : value;
                    OnPropertyChanged("Mv");
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            d12core = new d12_core();

            this.cb_aim.ItemsSource = d12core.Aim_Type;
            this.cb_move.ItemsSource = d12core.Mv_Type;

        }

        /// <summary>
        /// Проверить попадание при соблюдении условий
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_fire_Click(object sender, RoutedEventArgs e)
        {
            d12core.TargetType = (bool)this.rbtn_infantery.IsChecked ? 0 : ((bool)this.rbtn_vehicle.IsChecked ? 1 : 2);
            this.tb_result.Text = d12core.GetResult(this.cb_move.SelectedIndex, this.cb_aim.SelectedIndex);
            //this.tb_result.Text = d12core.GetResult(Mv, Aim);
        }

        /// <summary>
        /// Показать правила
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_rules_Click(object sender, RoutedEventArgs e)
        {
            d12core.TargetType = (bool)this.rbtn_infantery.IsChecked ? 0 : ((bool)this.rbtn_vehicle.IsChecked ? 2 : 3);
            this.tb_result.Text = string.Empty;
            StringBuilder sbR = new StringBuilder();

            switch (d12core.TargetType)
            {
                case 2:
                    {
                        #region для бронетехники
                        sbR.AppendLine("Система D12");
                        sbR.AppendLine("Автор: Борданов Антон");
                        sbR.AppendLine("---------------");
                        sbR.AppendLine("Имитация бровсков двенадцатигранных игральных костей.");
                        sbR.AppendLine("При помощи этой системы можно определить результат попадания.");
                        sbR.AppendLine("За каждой гранью закреплена некая пара результатов: хороший и плохой.");
                        sbR.AppendLine("Точность зависит от типа прицеливания и от перемещений юнитов в этот ход.");
                        sbR.AppendLine("Возможные игровые ситуации:");
                        sbR.AppendLine("1. Оба учатсница перестрелки стоят - Выбирается любой из всех результат");
                        sbR.AppendLine("2. Один из участников двигался перед стрельбой - Выбирается любой из плохих (список будет содержать только плохие результаты)");
                        sbR.AppendLine("3. Оба участника перестрелки двигались - Выбирается самый плохой результат.");
                        sbR.AppendLine(" ");
                        sbR.AppendLine("Стрельба по бронетехнике отличается от стрельбы по пехоте.");
                        sbR.AppendLine("Начать с того, что бронетехника имеет более тяжелую броню, непробиваемую личным стрелковым.");
                        sbR.AppendLine("Продолжить тем, что если техника ломается, то она ломается полность, не бывает чуть-чуть взорванного танка.");
                        sbR.AppendLine("Но! Такни имеют некую прочность брони, выражающуюся в хулл-поинтах.");
                        sbR.AppendLine("В связи с этим, попадания в танк имеют следующие результаты:");
                        sbR.AppendLine("Мимо - попадание не состоялось.");
                        sbR.AppendLine("Скользящее - снаряд скользнул по броне, но серьезных повреждений не причинил (краску поцарапал).");
                        sbR.AppendLine("Обычное - обычное попадание в броню. Бронелисты приняли удар. Происходит обычное снятие хулл-поинта.");
                        sbR.AppendLine("Критическое - попалание в тонкую часть. Как правило приводит к полному уничтожению техники, либо к серьезным негативным эффектам.");
                        sbR.AppendLine(" ");
                        sbR.AppendLine("Есть идея сделать отдельный генератор результатов для критических попаданий, основанный на D12.");
                        sbR.AppendLine("---------------");

                        #endregion для бронетехники
                        break;
                    }
                case 3:
                    {
                        #region для ПСИ
                        sbR.AppendLine("Система D12");
                        sbR.AppendLine("Автор: Борданов Антон");
                        sbR.AppendLine("---------------");
                        sbR.AppendLine("Имитация бровсков двенадцатигранных игральных костей.");
                        sbR.AppendLine("При помощи этой системы можно определить результат ПСИ-действия.");
                        sbR.AppendLine("За каждой гранью закреплена некая пара результатов: хороший и плохой.");
                        sbR.AppendLine("Эффективность ПСИ-воздействия (каста) зависит от молрали юнита.");
                        sbR.AppendLine("ПСИ-класс показывает количество бросаемых кубов на определение результата.");
                        sbR.AppendLine("Возможны следующие ситуации:");
                        sbR.AppendLine("1. Мораль кастующего больше морали цели.");
                        sbR.AppendLine("2. Мораль цели больше морали кастующего.");
                        sbR.AppendLine("3. Морали равны.");
                        sbR.AppendLine(" ");
                        sbR.AppendLine("Возможны следующие результаты:");
                        sbR.AppendLine("1. Критичесский провал - попадание в себя с критической интенсивностью каста (это не всегда плохо, например, в случае с лечением)");
                        sbR.AppendLine("2. Провал - попадание в себя (это не всегда плохо, например, в случае с лечением)");
                        sbR.AppendLine("3. Мискаст - Каст не достиг цели, либо не произошел");
                        sbR.AppendLine("4. Успех! - Каст произошел как нужно и достиг цели");
                        sbR.AppendLine("5. Критический Успех! - Каст достиг цели с критической интенсивностью.");
                        sbR.AppendLine("Применительно к ситуациям результат каста может быть следующим:");
                        sbR.AppendLine("1. Когда мораль кастующего больше морали цели, выбирается наилучший.");
                        sbR.AppendLine("2. Когда мораль кастующего равна морали цели, выбирается из худших. (Если стоит галочка мастера, то выбирается из хороших вариантов).");
                        sbR.AppendLine("3. Когда мораль кастующего меньше морали цели, наихудший.");
                        sbR.AppendLine("---------------");
                        #endregion для ПСИ
                        break;
                    }
                case 0:
                default:
                    {
                        #region для пехоты
                        sbR.AppendLine("Система D12");
                        sbR.AppendLine("Автор: Борданов Антон");
                        sbR.AppendLine("---------------");
                        sbR.AppendLine("Имитация бровсков двенадцатигранных игральных костей.");
                        sbR.AppendLine("При помощи этой системы можно определить результат попадания.");
                        sbR.AppendLine("За каждой гранью закреплена некая пара результатов: хороший и плохой.");
                        sbR.AppendLine("Точность зависит от типа прицеливания и от перемещений юнитов в этот ход.");
                        sbR.AppendLine("Возможные игровые ситуации:");
                        sbR.AppendLine("1. Оба учатсница перестрелки стоят - Выбирается любой из всех результат");
                        sbR.AppendLine("2. Один из участников двигался перед стрельбой - Выбирается любой из плохих (список будет содержать только плохие результаты)");
                        sbR.AppendLine("3. Оба участника перестрелки двигались - Выбирается самый плохой результат.");
                        sbR.AppendLine(" ");
                        sbR.AppendLine("В зависимости от того, как перемещались юниты, выбирается либо хороший, либо плохой результат.");
                        sbR.AppendLine("Количество бросаемых кубиков определяется типом прицеливания.");
                        sbR.AppendLine("Чем точнее выстрел, тем больше кубиков.");
                        sbR.AppendLine("---------------");
                        #endregion для пехоты
                        break;
                    }
            }

            this.tb_result.Text = sbR.ToString();
        }

        /// <summary>
        /// Вызвать окно для оценки результата рукопашной атаки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_hth_Click(object sender, RoutedEventArgs e)
        {
            d12_hth.Open();
        }

        /// <summary>
        /// Выбрать отыгрышь ПСИ-каста
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbtn_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)rbtn_psi.IsChecked)
            {
                this.cb_aim.ItemsSource = d12core.Dm_Type;
                this.cb_move.ItemsSource = d12core.PSI_skill;
                this.mk_01.Content = "Moral";
                this.mk_02.Content = "Skill";
            }
            else
            {
                this.cb_aim.ItemsSource = d12core.Aim_Type;
                this.cb_move.ItemsSource = d12core.Mv_Type;
                this.mk_01.Content = "Aim";
                this.mk_02.Content = "Move";
            }
        }
    }
}
