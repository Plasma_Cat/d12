﻿using System.Text;
using System.Windows;

namespace D12
{
    /// <summary>
    /// Логика взаимодействия для ab_hth.xaml
    /// </summary>
    public partial class d12_hth : Window
    {
        public d12_hth()
        {
            InitializeComponent();
        }

        public static void Open()
        {
            d12_hth One = new d12_hth();
            One.ShowDialog();
        }

        /// <summary>
        /// Показать правила
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_rls_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder strr = new StringBuilder();

            strr.AppendLine("Система Д12: рукопашный бой");
            strr.AppendLine("Автор: Антон Борданов");
            strr.AppendLine("---------------------");
            strr.AppendLine("Поскольку бой у нас происходит одновременный, то броски осуществляются одновременно.");
            strr.AppendLine("Порядок срабатывания определяется параметром инициативы или Реакции в нашем случае.");
            strr.AppendLine("У кого выше, того результаты будут рассматриваться в первую очередь.");
            strr.AppendLine("Запросто может быть так, что атакованный просто не смог нанести свои атаки, ибо вышел из боя из-за закончившихся хитов.");
            strr.AppendLine("Затем определяем кто на кого нападает.");
            strr.AppendLine("От этого зависит, какого качества результаты будут использоваться при подсчете.");
            strr.AppendLine("Скилл определяет сколько Д12 кидаем при атаке.");
            strr.AppendLine("Атак у обычного юнита за ход может быть одна на каждый слот действия. Итого, всего две.");
            strr.AppendLine("При попытке сбежать из боя например вторым слотом, юнит получает полноценную атаку в спину со всеми вытекающими.");
            strr.AppendLine("---------------------");
            strr.AppendLine("Нападающий выбирает из всех своих результатов ОДИН любой.");
            strr.AppendLine("Атакованный выбирает для блока/контратаки ОДИН из худших.");
            strr.AppendLine("Если направления Атаки и Защиты совпали, удар считается заблокированным и урон не проходит.");
            strr.AppendLine("Защищающийся может выбрать: блокировать или контатаковать.");
            strr.AppendLine("Если выбрана контратака, то повреждения проходят, а контратакующий выбирает результаты из худщих.");
            strr.AppendLine("Так же на порядок влияет инициатива.");
            strr.AppendLine("Если атакованый имеет реакцию выше, и выбирает блокировать, то он успевает подготовится и выбирает из лучших вариантов.");
            strr.AppendLine("Если выбирает контратаку, то отыгрывается его контратака, при которой он выбирает наихудший вариант.");
            strr.AppendLine("---------------------");
            strr.AppendLine("Например, Первый напал с Реакцией 2 на Второго с Реакцией 3.");
            strr.AppendLine("Второй объявил контратаку.");
            strr.AppendLine("Скилл Второго 3 - кидается 3хД12 и выбирается наихудший результат.");
            strr.AppendLine("А Первый уже обявил атаку и блокировать не может.");
            strr.AppendLine("В результате, сперва Первый огребает например в Руку, а затем он наносит удар в Ногу Второму.");
            strr.AppendLine("Кол-во списываемых при этом хитов, зависит от оружия и доп. условий. Но по базе - 1 атака сносит 1 хит.");
            strr.AppendLine("---------------------");
            strr.AppendLine("Короче:");
            strr.AppendLine("Атаует? - влияет на выбор результата. Если А, то выбирается из лучших.");
            strr.AppendLine("Инициатива? - влияет на выбор результата в блоке или конт-атаке. Если И<, то выбирается из худших.");
            strr.AppendLine("Навык - Определяет кол-во Д12.");

            this.tb_hthresult.Text = strr.ToString();
        }

        /// <summary>
        /// Получить результат рукопашной атаки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ch_hth_Click(object sender, RoutedEventArgs e)
        {
            int r1, r2, sk;
            bool ch = (bool)this.chb_a.IsChecked;
            if (int.TryParse(this.tb_react.Text, out r1) && int.TryParse(this.tb_react2.Text, out r2) && int.TryParse(this.tb_skill.Text, out sk))
                this.tb_hthresult.Text = MainWindow.d12core.GetResult(ch, r1, r2, sk);
            else
                this.tb_hthresult.Text = "Не удалось преобразовать введенные данные. Проверьте, в полях должны быть только цифры!";
        }

        /// <summary>
        /// Поменять местами значения реакция
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_swtch_Click(object sender, RoutedEventArgs e)
        {
            string tmp = tb_react.Text;
            this.tb_react.Text = this.tb_react2.Text;
            this.tb_react2.Text = tmp;
        }
    }
}
